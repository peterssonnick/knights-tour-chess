import React from 'react'
import './ChessTile.css';

const RED = 'RED';
const CYAN = 'CYAN';
const GRAY = 'GRAY';

const knightImg = require('../img/knight.png');

class ChessTile extends React.Component {

  render() {
    let tileColor = null;
    if (this.props.selected) {
      tileColor = RED;
    } else if (this.props.isLegalMove) {
      tileColor = CYAN;
    } else if (this.props.expired) {
      tileColor = this.props.funkyMode ? this.props.funkyColor : GRAY;
    } else {
      tileColor = this.props.row % 2 === 0 ? this.props.evenColor : this.props.oddColor
    }

    return (
        <span
          className='tile'
          onClick={()=>{!this.props.tourIsActive && this.props.onSelectTile(this.props.row, this.props.column)}}
          style={{ backgroundColor: tileColor }}
        >
          {this.props.numLegalMoves === 0 ? " " : this.props.numLegalMoves}
            <img
              className='tileImg'
              src={this.props.selected ? knightImg : ''}
              style={{ visibility: this.props.selected ? 'visible' : 'hidden' }}
            />
        </span>
    );
  }
}

export default ChessTile;
