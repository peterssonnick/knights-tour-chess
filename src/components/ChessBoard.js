import React from 'react';
import Tiles from '../util/chessTiles.js';
import ChessTile from './ChessTile.js';

const BLACK = 'BLACK';
const WHITE = 'WHITE';
const RED = 'RED';
const ORANGE = 'ORANGE';
const YELLOW = 'YELLOW';
const GREEN = 'GREEN';
const BLUE = 'BLUE';
const INDIGO = 'INDIGO';
const VIOLET = 'VIOLET';

const CYAN = 'CYAN';
const PURPLE = 'PURPLE';
const GRAY = 'GRAY';

const tileColors = [RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET];

class ChessBoard extends React.Component {

  state = {
    tiles: [],
    legalMoves: [],
    expiredTiles: [],
    funkyColor: RED,
    tourIsActive: false
  }

  componentDidMount = () => {
    let { tourUuid } = this.props;

    this.setState({
      tiles: Tiles.map((tile) => Object.assign({}, tile)),
      tourUuid: tourUuid
    });
  }

  componentDidUpdate = (prevProps) => {
    if (this.props.tourUuid !== prevProps.tourUuid) {
      this.resetBoard(this.props.tourUuid);
    }
  }

  resetBoard = (newTourUuid) => {
    this.setState({
      tiles: Tiles.map((tile) => Object.assign({}, tile)),
      legalMoves: [],
      expiredTiles: [],
      tourUuid: newTourUuid,
      tourIsActive: false
    });
  }

  getRandomColor = () => {
    return tileColors[Math.floor(Math.random()*tileColors.length)];
  }

  getNextColor = (currentColor) => {
    let result = null;
    if (currentColor != null) {
      tileColors.map((color, index) => {
        if (color === currentColor) {
          if (index + 1 < tileColors.length) {
            result = tileColors[index + 1];
          } else {
            result = tileColors[0];
          }
        }
      })
    } else {
      result = tileColors[0];
    }

    return result;
  }

  getLegalMoves = (row, column, selectedRow, selectedCol) => {
    let possibleSpots = this.state.tiles.filter(
      tile => !tile.expired && !tile.selected && !(tile.row == selectedRow && tile.column == selectedCol)
    );

    let legalMoves = [];
    possibleSpots.map((tile) => {
      if (tile.row === row + 2 && tile.column === column + 1) {
        legalMoves.push(tile);
      } else if (tile.row === row + 2 && tile.column === column - 1) {
        legalMoves.push(tile);
      } else if (tile.row === row + 1 && tile.column === column + 2) {
        legalMoves.push(tile);
      } else if (tile.row === row + 1 && tile.column === column - 2) {
        legalMoves.push(tile);
      } else if (tile.row === row - 2 && tile.column === column + 1) {
        legalMoves.push(tile);
      } else if (tile.row === row - 2 && tile.column === column - 1) {
        legalMoves.push(tile);
      } else if (tile.row === row - 1 && tile.column === column + 2) {
        legalMoves.push(tile);
      } else if (tile.row === row - 1 && tile.column === column - 2) {
        legalMoves.push(tile);
      }
    });

    return legalMoves.length;
  }

  onSelectTile = (row, column) => {
    let legalMoves = [];
    let funkyColor = this.getNextColor(this.state.funkyColor);

    let updatedTiles = this.state.tiles.map((tile) => {
      if (tile.row === row && tile.column === column) {
        if (!tile.selected) {
          tile.selected = true;
          tile.expired = true;
          // tile.funkyColor = this.getRandomColor();
          tile.funkyColor = funkyColor;
        }
      } else {
        tile.selected = false;
      }

      if (tile.row === row + 2 && tile.column === column + 1 && !tile.expired) {
        tile.isLegalMove = true;
        tile.numLegalMoves = this.getLegalMoves(tile.row, tile.column, row, column);
        legalMoves.push(tile);
      } else if (tile.row === row + 2 && tile.column === column - 1 && !tile.expired) {
        tile.isLegalMove = true;
        tile.numLegalMoves = this.getLegalMoves(tile.row, tile.column, row, column);
        legalMoves.push(tile);
      } else if (tile.row === row + 1 && tile.column === column + 2 && !tile.expired) {
        tile.isLegalMove = true;
        tile.numLegalMoves = this.getLegalMoves(tile.row, tile.column, row, column);
        legalMoves.push(tile);
      } else if (tile.row === row + 1 && tile.column === column - 2 && !tile.expired) {
        tile.isLegalMove = true;
        tile.numLegalMoves = this.getLegalMoves(tile.row, tile.column, row, column);
        legalMoves.push(tile);
      } else if (tile.row === row - 2 && tile.column === column + 1 && !tile.expired) {
        tile.isLegalMove = true;
        tile.numLegalMoves = this.getLegalMoves(tile.row, tile.column, row, column);
        legalMoves.push(tile);
      } else if (tile.row === row - 2 && tile.column === column - 1 && !tile.expired) {
        tile.isLegalMove = true;
        tile.numLegalMoves = this.getLegalMoves(tile.row, tile.column, row, column);
        legalMoves.push(tile);
      } else if (tile.row === row - 1 && tile.column === column + 2 && !tile.expired) {
        tile.isLegalMove = true;
        tile.numLegalMoves = this.getLegalMoves(tile.row, tile.column, row, column);
        legalMoves.push(tile);
      } else if (tile.row === row - 1 && tile.column === column - 2 && !tile.expired) {
        tile.isLegalMove = true;
        tile.numLegalMoves = this.getLegalMoves(tile.row, tile.column, row, column);
        legalMoves.push(tile);
      } else {
        tile.isLegalMove = false;
        tile.numLegalMoves = 0;
      }

      if (legalMoves.length != 0) {
        setTimeout(() => this.tour(legalMoves), this.props.tourSpeed);
      }

      return tile;
    });

    this.setState({
      tiles: updatedTiles,
      funkyColor: funkyColor,
      tourIsActive: true
    })
  }

  tour = (legalMoves) => {
    let minMoves = 8;
    let nextMove = null;
    let { tourIsActive } = this.state;

    legalMoves.map((legalMove) => {
      if (legalMove.numLegalMoves < minMoves && legalMove.numLegalMoves > 0) {
        minMoves = legalMove.numLegalMoves;
        nextMove = legalMove;
      }
    });

    if (nextMove && tourIsActive) {
      this.onSelectTile(nextMove.row, nextMove.column);
    }
  }

  render() {
    let currCol = 1;
    let evenColor = BLACK;
    let oddColor = WHITE;

    return this.state.tiles.map((tile) =>  {
      if (currCol != tile.column) {
        currCol = tile.column;
        evenColor = evenColor === WHITE ? BLACK : WHITE;
        oddColor = oddColor === WHITE ? BLACK : WHITE;
        return (
          <span key={`tile-${tile.row}-${tile.column}`}>
            <br/>
            <ChessTile
              row={tile.row}
              column={tile.column}
              occupied={tile.occupied}
              selected={tile.selected}
              expired={tile.expired}
              evenColor={evenColor}
              oddColor={oddColor}
              isLegalMove={tile.isLegalMove}
              numLegalMoves={tile.numLegalMoves}
              legalMoves={this.state.legalMoves}
              onSelectTile={this.onSelectTile}
              funkyMode={this.props.funkyMode}
              funkyColor={tile.funkyColor}
              tourIsActive={this.state.tourIsActive}
            />
          </span>
        )
      } else {
        return (
          <span key={`tile-${tile.row}-${tile.column}`}>
            <ChessTile
              row={tile.row}
              column={tile.column}
              occupied={tile.occupied}
              selected={tile.selected}
              expired={tile.expired}
              evenColor={evenColor}
              oddColor={oddColor}
              isLegalMove={tile.isLegalMove}
              numLegalMoves={tile.numLegalMoves}
              legalMoves={this.state.legalMoves}
              onSelectTile={this.onSelectTile}
              funkyMode={this.props.funkyMode}
              funkyColor={tile.funkyColor}
              tourIsActive={this.state.tourIsActive}
            />
          </span>
        )
      }
    });
  }
}

export default ChessBoard;
