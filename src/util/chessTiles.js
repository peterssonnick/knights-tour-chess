module.exports =
[
  {
    column: 1,
    row: 1,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0,
  },
  {
    column: 1,
    row: 2,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0,
  },
  {
    column: 1,
    row: 3,
    isLegalMove: false,
    selected: false, 
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 1,
    row: 4,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 1,
    row: 5,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 1,
    row: 6,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 1,
    row: 7,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 1,
    row: 8,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 2,
    row: 1,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 2,
    row: 2,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 2,
    row: 3,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 2,
    row: 4,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 2,
    row: 5,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 2,
    row: 6,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 2,
    row: 7,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 2,
    row: 8,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 3,
    row: 1,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 3,
    row: 2,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 3,
    row: 3,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 3,
    row: 4,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 3,
    row: 5,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 3,
    row: 6,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 3,
    row: 7,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 3,
    row: 8,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 4,
    row: 1,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 4,
    row: 2,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 4,
    row: 3,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 4,
    row: 4,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 4,
    row: 5,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 4,
    row: 6,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 4,
    row: 7,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 4,
    row: 8,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 5,
    row: 1,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 5,
    row: 2,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 5,
    row: 3,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 5,
    row: 4,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 5,
    row: 5,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 5,
    row: 6,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 5,
    row: 7,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 5,
    row: 8,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 6,
    row: 1,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 6,
    row: 2,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 6,
    row: 3,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 6,
    row: 4,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 6,
    row: 5,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 6,
    row: 6,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 6,
    row: 7,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 6,
    row: 8,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 7,
    row: 1,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 7,
    row: 2,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 7,
    row: 3,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 7,
    row: 4,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 7,
    row: 5,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 7,
    row: 6,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 7,
    row: 7,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 7,
    row: 8,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 8,
    row: 1,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 8,
    row: 2,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 8,
    row: 3,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 8,
    row: 4,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 8,
    row: 5,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 8,
    row: 6,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 8,
    row: 7,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  },
  {
    column: 8,
    row: 8,
    isLegalMove: false,
    selected: false,
    expired: false,
    numLegalMoves: 0
  }
]
