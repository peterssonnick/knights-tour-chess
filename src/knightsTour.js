import React from 'react';
import ChessBoard from './components/ChessBoard.js';
import Slider from 'react-rangeslider';
import Switch from 'react-toggle-switch';
import '../node_modules/react-toggle-switch/dist/css/switch.min.css';
import { v4 as uuidv4 } from 'uuid';

const min = 1;
const max = 8;
const maxTimeout = 1000;
const timeoutSpeed = 2500;

class KnightsTour extends React.Component {
  static displayName = "knightsTour";

  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      funkyMode: false,
      tourUuid: uuidv4()
    }
  }

  handleChange = value => {
    this.setState({
      value: value
    })
  };

  toggleSwitch = value => {
    this.setState(prevState => {
      return {
        funkyMode: !prevState.funkyMode
      }
    })
  }

  handleReset = () => {
    this.setState({
      tourUuid: uuidv4()
    });
  }

  render() {
    return (
    <div>
      {/*<div className='slider'>
        Tour Speed:
        <Slider
          min={0}
          max={100}
          value={this.state.value}
          onChange={this.handleChange}
        />
      </div>*/}
      {/*<div>
        <h3>Funky Mode</h3>
        <Switch
          onClick={this.toggleSwitch}
          on={this.state.funkyMode}
        />
      </div>*/}
        <div style={{ width: '100%', height: '100%' }}>
          <h3>Knight's Tour</h3>
          <p>Select any position to begin the tour</p>
          <ChessBoard
            tourSpeed={maxTimeout - (this.state.value * 30)}
            funkyMode={this.state.funkyMode}
            tourUuid={this.state.tourUuid}
          />
          <br/>
          <button
            onClick={this.handleReset}
            style={{ marginTop: '15px' }}
          >
            Reset
          </button>
          <p>Learn more about the Knight's Tour <a href="https://en.wikipedia.org/wiki/Knight%27s_tour">here</a></p>
        </div>
    </div>
  )}
};

export default KnightsTour;
