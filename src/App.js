import React, { Component } from 'react';
import KnightsTour from './knightsTour.js'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <KnightsTour />
      </div>
    );
  }
}

export default App;
